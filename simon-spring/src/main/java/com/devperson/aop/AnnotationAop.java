package com.devperson.aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 功能描述: 纯注解方式开发切面
 *
 * @author lixiaomeng
 * @date 2018/11/26
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.devperson.aop")
public class AnnotationAop {


}
