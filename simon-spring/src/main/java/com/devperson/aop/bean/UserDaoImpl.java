package com.devperson.aop.bean;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
public class UserDaoImpl {

    public int insterUser() {
        System.out.println("插入一条数据");
        return 1;
    }

    public void insterUserVoid() {
        System.out.println("插入一条数据,返回数据为空");
    }

}
