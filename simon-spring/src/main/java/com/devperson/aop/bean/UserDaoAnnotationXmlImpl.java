package com.devperson.aop.bean;

import org.springframework.stereotype.Component;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/26
 */
@Component
public class UserDaoAnnotationXmlImpl {
//    private static final Logger logger = Lo

    public int annotationinster() {
        System.out.println("基于XML和Annotation的AOP开发方式");
        return 1;
    }

}
