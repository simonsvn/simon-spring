package com.devperson.aop;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * 功能描述: aop类
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
public class MyXMLAdvice {

    public void beforeLog() {
        System.out.println("AOP前置，方法执行之前输出日志");
    }

    public void afterLog() {
        System.out.println("AOP后置通知，方法执行之后输出日志");
    }

    public Object aroundMethod(ProceedingJoinPoint joinPoint) {
        Long times = System.currentTimeMillis();
        Object resultValue = null;
        System.out.println("这是环绕通知:" + times);
        try {
            resultValue =  joinPoint.proceed(joinPoint.getArgs());
            System.out.println("方法执行了：" + (System.currentTimeMillis() - times) + "毫秒");
            System.out.println("方法执行完成以后，输出");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return resultValue;

    }

}
