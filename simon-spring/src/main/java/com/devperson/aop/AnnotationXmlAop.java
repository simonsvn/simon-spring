package com.devperson.aop;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 功能描述: 注解方式定义切面bean
 *
 * @author lixiaomeng
 * @date 2018/11/26
 */
@Component
@Aspect
public class AnnotationXmlAop {

    @Before(value = "execution(* com.devperson.aop.bean.*AnnotationXmlImpl.*inster*(..))")
    public void annoAopBean() {
        System.out.println("基于注解和XML混合模式的开发，before切面");
    }

    @AfterReturning(value = "execution(* com.devperson.aop.bean.*AnnotationXmlImpl.*inster*(..))")
    public void annXmlAopAfter() {
        System.out.println("基于注解和XML混合模式的开发，AfterReturning切面");
    }
}
