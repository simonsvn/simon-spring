package com.devperson.di;

import com.devperson.xml.SessionBean;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 功能描述: set方式注入，需要在bean里面，添加set方法
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class SetDIBeanByHand {

    private String name;

    private int age;

    private SessionBean sessionBean;

    private List<String> children;

    private Map house;

    private Set car;

    private Properties properties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public SessionBean getSessionBean() {
        return sessionBean;
    }

    public void setSessionBean(SessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }

    public List<String> getChildren() {
        return children;
    }

    public void setChildren(List<String> children) {
        this.children = children;
    }

    public Map getHouse() {
        return house;
    }

    public void setHouse(Map house) {
        this.house = house;
    }

    public Set getCar() {
        return car;
    }

    public void setCar(Set car) {
        this.car = car;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "SetDIBeanByHand{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sessionBean=" + sessionBean +
                ", children=" + children +
                ", house=" + house +
                ", car=" + car +
                ", properties=" + properties +
                '}';
    }
}
