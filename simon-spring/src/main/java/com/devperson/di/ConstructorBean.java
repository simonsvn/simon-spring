package com.devperson.di;

import com.devperson.xml.SessionBean;

/**
 * 功能描述: 构造器注入
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class ConstructorBean {

    private String name;

    private int age;

    private SessionBean sessionBean;

    public ConstructorBean(String name, int age, SessionBean sessionBean) {
        this.name = name;
        this.age = age;
        this.sessionBean = sessionBean;
    }

    @Override
    public String toString() {
        return "ConstructorBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sessionBean=" + sessionBean +
                '}';
    }
}
