package com.devperson.annotation;

import org.springframework.beans.factory.annotation.Value;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
public class JdbcBean {

    @Value("${jdbc.url}")
    private String url;

    @Value("${jdbc.username}")
    private String userName;

    @Value("${jdbc.password}")
    private String passwd;

    @Value("${jdbc.driver}")
    private String driver;

    @Override
    public String toString() {
        return "JdbcBean{" +
                "url='" + url + '\'' +
                ", userName='" + userName + '\'' +
                ", passwd='" + passwd + '\'' +
                ", driver='" + driver + '\'' +
                '}';
    }
}
