package com.devperson.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
@Configuration
@PropertySource(value = "classpath:jdbc.properties")
public class JDBCConfig {

    @Bean
    public JdbcBean jdbcBean() {
        JdbcBean jdbcBean = new JdbcBean();
        return jdbcBean;
    }

}
