package com.devperson.annotation;

import org.springframework.beans.factory.annotation.Value;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
public class PropertyBean {

    @Value("${annotation.name}")
    private String name;

    @Value("${annotation.age}")
    private int age;

    public String annotation() {
        System.out.println(this.toString());
        return this.getClass().getCanonicalName();
    }

    @Override
    public String toString() {
        return "PropertyBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
