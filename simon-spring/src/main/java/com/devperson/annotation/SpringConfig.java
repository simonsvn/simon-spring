package com.devperson.annotation;

import org.springframework.context.annotation.*;

/**
 * 功能描述: Spring注解方式
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
@Configuration
@ComponentScans({
        @ComponentScan(basePackages = {
                "com.devperson","com.devother"
        } )
})
@PropertySource(value = "classpath:spring.properties", encoding = "UTF-8")
@Import({
        JDBCConfig.class
})
public class SpringConfig {

    @Bean
    public AnnotationBean annotationBean() {
        AnnotationBean annotationBean = new AnnotationBean();
        return annotationBean;
    }

    @Bean
    public PropertyBean propertyBean() {
        PropertyBean propertyBean = new PropertyBean();
        return propertyBean;
    }

}
