package com.devperson.annotation;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
public class AnnotationBean {

    public String annotation() {
        return this.getClass().getCanonicalName();
    }

}
