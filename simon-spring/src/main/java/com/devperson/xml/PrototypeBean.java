package com.devperson.xml;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class PrototypeBean {

    public String beanAddr() {
        System.out.println(this.toString());
        return this.toString();
    }

    public void init() {
        System.out.println("init......PrototypeBean");
    }

    public void destory() {
        System.out.println("destory......PrototypeBean");
    }
}
