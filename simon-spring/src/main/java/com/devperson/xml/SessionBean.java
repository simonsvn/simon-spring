package com.devperson.xml;

/**
 * 功能描述: spring的Session級別的bean管理，session级别的bean，存在于web项目
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class SessionBean {

    public String thisBeanAddr() {
        System.out.println(this);
        return this.toString();
    }

    public void init() {
        System.out.println("init....SessionBean");
    }
}
