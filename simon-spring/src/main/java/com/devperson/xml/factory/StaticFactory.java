package com.devperson.xml.factory;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class StaticFactory {

    public static StaticBeanByFactory createBeanByStatic() {
        StaticBeanByFactory staticBeanByFactory = new StaticBeanByFactory();

        return staticBeanByFactory;
    }
}
