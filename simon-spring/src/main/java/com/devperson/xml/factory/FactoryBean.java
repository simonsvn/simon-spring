package com.devperson.xml.factory;

/**
 * 功能描述: 通过bean工厂生成Bean
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class FactoryBean {

    public String beanClassName() {
        return this.getClass().getCanonicalName();
    }

}
