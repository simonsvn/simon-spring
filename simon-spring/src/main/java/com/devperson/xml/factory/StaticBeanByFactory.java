package com.devperson.xml.factory;

/**
 * 功能描述: 通过静态工厂生成Bean
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
public class StaticBeanByFactory {

    public String beanMethod() {
        return this.getClass().getCanonicalName();
    }

}
