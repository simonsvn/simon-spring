package com.devperson.xmlAndAnnotation.annotation;

import org.springframework.stereotype.Service;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@Service
public class ServiceAnnotation {

    public String annotation() {
        System.out.println(this.getClass().getCanonicalName());
        return this.getClass().getAnnotations().toString();
    }
}
