package com.devperson.xmlAndAnnotation.annotation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@Component
public class ComponentAnnotation {

    @Value("${name}")
    private String name;

    @Value("${age}")
    private int age;


    @PostConstruct
    public void annotationInit() {

    }


    public String annotation() {
        System.out.println(this.toString());
        System.out.println(this.getClass().getCanonicalName());
        return this.getClass().getAnnotations().toString();
    }

    @Override
    public String toString() {
        return "ComponentAnnotation{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
