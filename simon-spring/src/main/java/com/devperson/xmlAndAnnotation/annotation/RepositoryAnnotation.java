package com.devperson.xmlAndAnnotation.annotation;

import org.springframework.stereotype.Repository;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@Repository
public class RepositoryAnnotation {
    public String annotation() {
        System.out.println(this.getClass().getCanonicalName());
        return this.getClass().getAnnotations().toString();
    }
}
