package com.devperson.xmlAndAnnotation.annotation;

import org.springframework.stereotype.Controller;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@Controller
public class ControllerAnnotation {

    public String annotation() {
        System.out.println(this.getClass().getCanonicalName());
        return this.getClass().getAnnotations().toString();
    }
}
