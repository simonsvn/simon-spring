package com.devperson.testBean;

import com.devperson.aop.bean.UserDaoAnnotationXmlImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/26
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-xmlaop.xml"
})
public class TestAnnotationXmlAAop {

    @Autowired
    UserDaoAnnotationXmlImpl userDaoAnnotationXml;

    @Test
    public void testInster(){

        System.out.println(userDaoAnnotationXml.annotationinster());
    }

}
