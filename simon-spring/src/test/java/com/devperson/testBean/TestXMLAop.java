package com.devperson.testBean;

import com.devperson.aop.bean.UserDaoImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-aop.xml"
})
public class TestXMLAop {

    @Autowired
    private UserDaoImpl userDao;

    @Test
    public void testUserBefore() {
        System.out.println(userDao.insterUser());
    }

    @Test
    public void testInsterVoid() {
        userDao.insterUserVoid();
        System.out.println("这是测试执行完毕");
    }

}
