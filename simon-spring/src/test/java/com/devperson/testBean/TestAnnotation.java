package com.devperson.testBean;

import com.devperson.annotation.AnnotationBean;
import com.devperson.annotation.JdbcBean;
import com.devperson.annotation.PropertyBean;
import com.devperson.annotation.SpringConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/23
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
public class TestAnnotation {

    @Autowired
    AnnotationBean annotationBean;

    @Autowired
    PropertyBean propertyBean;

    @Autowired
    JdbcBean jdbcBean;

    @Test
    public void testAnnotation() {
        System.out.println(annotationBean.annotation());
    }

    @Test
    public void testProperty() {
        System.out.println(propertyBean.annotation());
    }

    @Test
    public void testJdbc() {
        System.out.println(jdbcBean.toString());
    }

}
