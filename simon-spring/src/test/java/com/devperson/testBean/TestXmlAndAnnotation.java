package com.devperson.testBean;

import com.devperson.xmlAndAnnotation.annotation.ComponentAnnotation;
import com.devperson.xmlAndAnnotation.annotation.ServiceAnnotation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-appliaction.xml"
})
public class TestXmlAndAnnotation {

    @Autowired
    private ServiceAnnotation serviceAnnotation;

    @Autowired
    private ComponentAnnotation componentAnnotation;

    @Test
    public void testServiceAnnotation() {
        serviceAnnotation.annotation();
    }

    @Test
    public void testComponeAnnotation() {
        componentAnnotation.annotation();
    }
}
