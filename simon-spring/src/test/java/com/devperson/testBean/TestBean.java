package com.devperson.testBean;

import com.devperson.xml.PrototypeBean;
import com.devperson.di.ConstructorBean;
import com.devperson.di.SetDIBeanByHand;
import com.devperson.xml.factory.FactoryBean;
import com.devperson.xml.factory.StaticBeanByFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/22
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-appliaction.xml",
        "classpath:spring-beans.xml"
})
public class TestBean {

    @Resource
    private PrototypeBean prototypeBean;

    @Resource
    StaticBeanByFactory staticBeanByFactory;

    @Autowired
    FactoryBean factoryBean;

    @Test
    public void testPrototyBean() {
        prototypeBean.beanAddr();
        System.out.println("再次调用------");
        prototypeBean.beanAddr();
    }

    @Test
    public void testFactoryStaticBean() {
        System.out.println(staticBeanByFactory.beanMethod());
    }

    @Test
    public void testFactoryBean() {
        // String beanName = factoryBean.beanClassName();
        System.out.println(factoryBean.beanClassName());
    }

    @Autowired
    private ConstructorBean constructorBean;

    @Test
    public void testConstructor() {
        System.out.println(constructorBean.toString());
    }

    @Autowired
    private SetDIBeanByHand setDIBeanByHand;

    @Test
    public void testSetDIBeanByHand() {
        System.out.println(setDIBeanByHand.toString());
    }



}
