package com.devperson.controlleradvice;

import com.devperson.exception.ControllerException;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:
 * 该注解内部使用@ExceptionHandler、@InitBinder、@ModelAttribute
 * 注解的方法会应用到所有的Controller类中 @RequestMapping注解的方法
 *
 * @author lixiaomeng
 * @date 2018/10/29
 */
@ControllerAdvice
public class MyControllerAdvice {

    /**
     *
     * 功能描述: ModelAttribute注解，应用到所有@RequestMapping注解方法，
     * 在其执行之前把返回值放入ModelMap中
     * modeAttr()方法可以接收Model对象的参数，即默认支持的
     * HttpServletRequest
     * HttpServletResponse
     * HttpSession
     * Model/ModelMap
     * @param: []
     * @return: java.util.Map
     * @author: lixiaomeng
     * @time: 2018/10/29 19:32
     */
    @ModelAttribute
    public Map modeAttr(Model model) {
        Map map = new HashMap();
        map.put("msg","ModelAttribute");
        model.addAttribute("modelattr", "啊啊啊");
        return map;
    }

    /**
     * @InitBinder 注解，会应用到所有带参数的requestMapping方法中，在参数绑定之前，调用该方法
     * @param webDataBinder
     */
    @InitBinder
    public void initbinder(WebDataBinder webDataBinder) {
        System.out.println("webDataBinder.....");
        System.out.println(webDataBinder.getDisallowedFields());
    }


    /**
     * 异常在控制增强，controller抛出对应的异常时，调用该方法
     * @param e
     * @return
     */
    @ExceptionHandler(ControllerException.class)
    public String exceptionAdvice(Exception e) {
        System.out.println("advice Exception。。。。。");
        e.printStackTrace();

        return "hello";
    }
}
