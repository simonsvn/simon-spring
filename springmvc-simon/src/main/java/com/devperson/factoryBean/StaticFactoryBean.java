package com.devperson.factoryBean;

import com.devperson.factoryBean.beanClass.FactoryBean;

public class StaticFactoryBean {

    public static FactoryBean factoryBean() {
        System.out.println("StaticFactoryBean.factoryBean，静态工厂");
        FactoryBean factoryBean = new FactoryBean();
        factoryBean.setName("这是bean静态工厂生成的类");
        return factoryBean;
    }
}
