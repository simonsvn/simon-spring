package com.devperson.factoryBean;

import com.devperson.factoryBean.beanClass.BeanInstance;

public class BeanFactoryInstance {

    public BeanInstance factoryBean() {
        System.out.println("BeanFactoryInstance.factoryBean工厂");
        BeanInstance factoryBean = new BeanInstance();
        factoryBean.setName("这是bean工厂生成的类");
        return factoryBean;
    }
}
