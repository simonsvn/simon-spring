package com.devperson.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/10/29
 */
@Controller
public class AdviceController {

    @RequestMapping("modelAttributeAdviceModel")
    public ModelAndView modelAttributeAdviceModel(Model model) {
        ModelAndView modelAndView = new ModelAndView("hello");

        Map<String, Object> map = model.asMap();
        for (Map.Entry<String, Object> entry: map.entrySet()) {
            System.out.println(entry.getKey() + "--> value==" + entry.getValue());
        }

        return modelAndView;
    }

}
