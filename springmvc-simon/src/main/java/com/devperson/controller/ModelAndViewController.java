package com.devperson.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述: 测试SpringMVC处理器的返回值
 *
 * @author lixiaomeng
 * @date 2018/10/26
 */
@Controller
public class ModelAndViewController {

    /* *
     *
     * 功能描述: 处理器返回字符串类型
     *
     * @param: [model] Model对象是Spring默认内置支持的对象，model中用来将数据存储到Request域中
     * @return: java.lang.String 返回字符串形式的响应试图。
     * 返回值，通过org.springframework.web.servlet.view.InternalResourceViewResolver试图处理器，进行解析
     * <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
     *  <property name="prefix" value="WEB-INF/page/"/>
     * <property name="suffix" value=".jsp"/>
     * </bean>
     * 在返回字符串前后，添加历经信息及后缀
     * @author: lixiaomeng
     * @time: 2018/10/26 15:47
     */
    @RequestMapping("hello_simon")
    public String hello(Model model) {
        System.out.println("hello_simon.....");
        model.addAttribute("msg","这是MVC的model属性对象");
        return "hello";
    }

    /* *
     *
     * 功能描述: 处理器返回ModelAndView对象： model表示的是数据模型，view就是最终要展示给用户的视图
     * 该对象中，包含了用户展示试图和返回给用户的数据
     * @param: []
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 15:53
     */
    @RequestMapping("helloModel")
    public ModelAndView helloModel() {

        // 返回的view对象，可以理解为页面
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println("helloModel.....");
        modelAndView.addObject("msg","这是ModelAndView返回的内容");
        return modelAndView;
    }
}
