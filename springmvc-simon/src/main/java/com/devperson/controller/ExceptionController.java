package com.devperson.controller;

import com.devperson.exception.ControllerException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/10/29
 */
@Controller
public class ExceptionController {

    /**
     *
     * 功能描述: 异常处理机制
     *
     * @param: []
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/29 19:21
     */
    @RequestMapping("throwException")
    public ModelAndView throwException() throws Exception {
        ModelAndView modelAndView = new ModelAndView("hello");
        if (1==1)
            throw new ControllerException("自定义抛出异常!");
        return modelAndView;
    }

    /**
    *
    * 功能描述:
    *
    * @param: []
    * @return: org.springframework.web.servlet.ModelAndView
    * @author: lixiaomeng
    * @time: 2018/10/29 19:24
    */
    @RequestMapping("throwUndefindException")
    public ModelAndView throwUndefindException() throws Exception {
        ModelAndView modelAndView = new ModelAndView("hello");
        if (1==1)
            throw new NullPointerException();
        return modelAndView;
    }

}
