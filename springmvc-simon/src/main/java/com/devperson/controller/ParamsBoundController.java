package com.devperson.controller;

import com.devperson.vo.BaseTypeVo;
import com.devperson.vo.DateVo;
import com.devperson.vo.FiledClassVo;
import com.devperson.vo.ListBoundVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 功能描述: 参数绑定
 *
 * @author lixiaomeng
 * @date 2018/10/26
 */
@Controller
public class ParamsBoundController {

//    @Autowired
//    ServiceImpl service;

    /**
     *
     * 功能描述: 测试基本类型绑定
     * GET绑定方式： http://localhost:8088/baseTypeBound?str=str&inter=1&bool=1&doub=3.44&floa=5.01
     * POST的参数绑定（form表单数据提交）
     * @param: [str, inter, bool, doub, floa]
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 16:08
     */
    @RequestMapping("baseTypeBound")
    public ModelAndView baseTypeBound(String str, int inter, boolean bool, double doub, float floa) {
        ModelAndView modelAndView = new ModelAndView("hello");
        modelAndView.addObject("msg", "str:" + str + "  inter: " + inter +
                " bool: " + bool + " doub: " + doub + "floa : " + floa);
        return modelAndView;
    }

    /**
     *
     * 功能描述: POJO类，接收基本类型参数
     *
     * @param: [baseTypeVo]
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 16:54
     */
    @RequestMapping("classTypeBound")
    public ModelAndView classTypeBound(BaseTypeVo baseTypeVo) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println(baseTypeVo.toString());
        modelAndView.addObject("msg", baseTypeVo.toString());
        return modelAndView;
    }


    /**
     *
     * 功能描述: 接收json类型的参数,绑定普通类型，json类型的参数，需要json包的解析配置
     * spring默认使用的是jackson-databind，所以如果使用json的参数类型，需要添加该包的引用
     *
     * @param: [baseTypeVo]
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 17:00
     */
    @RequestMapping("classTypeBoundByJson")
    public ModelAndView classTypeBoundByJson( @RequestBody BaseTypeVo baseTypeVo) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println(baseTypeVo.toString());
        modelAndView.addObject("msg", baseTypeVo.toString());
        return modelAndView;
    }


    /**
     *
     * 功能描述: FiledClassVo 包装类，绑定包装类中的属性，BaseTypeVo类
     * @param: [filedClassVo]
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 17:00
     */
    @RequestMapping("filedClassType")
    public ModelAndView filedClassType(FiledClassVo filedClassVo) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println("filedClassType:" + filedClassVo.toString());
        modelAndView.addObject("msg", filedClassVo);
        return modelAndView;
    }


    /**
     *
     * 功能描述: FiledClassVo 包装类，绑定包装类中的属性，BaseTypeVo类, 参数树JSON数据格式
     * @param: [filedClassVo]
     * @return: org.springframework.web.servlet.ModelAndView
     * @author: lixiaomeng
     * @time: 2018/10/26 17:00
     **/
    @RequestMapping("filedClassTypeJson")
    public ModelAndView filedClassTypeJson( @RequestBody FiledClassVo filedClassVo) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println("filedClassType:" + filedClassVo.toString());
        modelAndView.addObject("msg", filedClassVo);
        return modelAndView;
    }

    /**
     * 简单类型的数组参数绑定
     * */
    @RequestMapping("arrayParamsBound")
    public ModelAndView arrayParamsBound(String[] ids) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println(ids.length + "--> "  + ids[0]);
        modelAndView.addObject("msg",ids);

        return modelAndView;
    }

    /**
     * 集合类型的参数绑定
     * */
    @RequestMapping("classListBound")
    public ModelAndView classListBound(ListBoundVo listBoundVo) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println(listBoundVo);
        modelAndView.addObject("msg",listBoundVo);
        return modelAndView;
    }



    /**
     * 测试日期类型参数绑定
     *
     * */
    @RequestMapping("dataTypeBound")
    public ModelAndView dataTypeBound(Date date) {
        ModelAndView modelAndView = new ModelAndView("hello");
        System.out.println(date);
        modelAndView.addObject("msg",date);
        return modelAndView;
    }

    /**
     * 文件上傳
     * */
    @RequestMapping("fileUpload")
    public ModelAndView fileUpload(MultipartFile files) throws IOException {
        ModelAndView modelAndView = new ModelAndView("hello");
        if (files != null) {
            System.out.println(files.getName());
            System.out.println(files.getBytes());
            System.out.println(files.getSize());
            String fileName = System.currentTimeMillis() + files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf("."));
            File newFile = new File(fileName);
            files.transferTo(newFile);
            System.out.println("上傳成功!");
            System.out.println(newFile.getPath());
            System.out.println(newFile.getAbsolutePath());
            modelAndView.addObject("mag",fileName);
        }
        System.out.println("........");
        return modelAndView;
    }

    /**
     * 返回json字符串到页面
     * */
    @RequestMapping("returnJsonString")
    @ResponseBody
    public DateVo returnJsonString() {
        DateVo dateVo = new DateVo();
        dateVo.setName("Simon");
        dateVo.setDate(new Date());
        List<String> list = new ArrayList<>();
//        list.add("aa");
//        list.add("aa");
//        list.add("aa");
//        list.forEach(s -> {
//            try {
//                service.testException();
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//        });

        return dateVo;
    }


}
