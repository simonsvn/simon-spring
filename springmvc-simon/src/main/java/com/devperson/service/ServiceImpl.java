package com.devperson.service;

import com.devperson.controller.ParamsBoundController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/13
 */
@Service
public class ServiceImpl {

    @Autowired
    ParamsBoundController paramsBoundController;

    public void testExceptionThrow() throws Exception {
        throw new RuntimeException("aa");
    }

    public void testException()  {
        try {
            paramsBoundController.returnJsonString();
            throw new RuntimeException("aa");
        }catch (Exception e) {
            System.out.println("-----------*****");
            throw new RuntimeException(e);
        }

    }
}
