package com.devperson.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListBoundVo implements Serializable {

    private List<BaseTypeVo> item = new ArrayList<>();

    private String name;

    public List<BaseTypeVo> getItem() {
        return item;
    }

    public void setItem(List<BaseTypeVo> item) {
        this.item = item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ListBoundVo{" +
                "item=" + item +
                ", name='" + name + '\'' +
                '}';
    }
}
