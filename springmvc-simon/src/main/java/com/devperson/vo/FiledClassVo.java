package com.devperson.vo;

import java.io.Serializable;
import java.util.Date;

public class FiledClassVo implements Serializable {

    private BaseTypeVo baseTypeVo;

    private String name;

    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BaseTypeVo getBaseTypeVo() {
        return baseTypeVo;
    }

    public void setBaseTypeVo(BaseTypeVo baseTypeVo) {
        this.baseTypeVo = baseTypeVo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FiledClassVo{" +
                "baseTypeVo=" + baseTypeVo +
                ", name='" + name + '\'' +
                ", date=" + date +
                '}';
    }
}
