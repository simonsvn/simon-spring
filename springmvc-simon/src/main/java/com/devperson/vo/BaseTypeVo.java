package com.devperson.vo;

import java.io.Serializable;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/10/26
 */
public class BaseTypeVo implements Serializable {

    private String str;
    private Integer inter;
    private boolean bool;
    private double doub;
    private float floa;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Integer getInter() {
        return inter;
    }

    public void setInter(Integer inter) {
        this.inter = inter;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public double getDoub() {
        return doub;
    }

    public void setDoub(double doub) {
        this.doub = doub;
    }

    public float getFloa() {
        return floa;
    }

    public void setFloa(float floa) {
        this.floa = floa;
    }

    @Override
    public String toString() {
        return "BaseTypeVo{" +
                "str='" + str + '\'' +
                ", inter=" + inter +
                ", bool=" + bool +
                ", doub=" + doub +
                ", floa=" + floa +
                '}';
    }
}
