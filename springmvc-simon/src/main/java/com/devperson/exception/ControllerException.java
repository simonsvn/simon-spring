package com.devperson.exception;

/**
 * 功能描述: controller异常
 *
 * @author lixiaomeng
 * @date 2018/10/29
 */
public class ControllerException extends Exception {

    private static final long serialVersionUID = 1L;

    public String message;

    public ControllerException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
