package com.devperson.exception.resolver;

import com.devperson.exception.ControllerException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述: 异常处理器
 *
 * @author lixiaomeng
 * @date 2018/10/29
 */
public class ApplicationExceptionResolver implements HandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
                                         Object handler, Exception ex) {
        ModelAndView modelAndView = new ModelAndView("error");
        if (ex instanceof ControllerException) {
            modelAndView.addObject("msg", ex.getMessage());
        } else {
            modelAndView.addObject("msg", "未知异常处理");
        }
        return modelAndView;
    }
}
