package com.devperson;

import com.devperson.service.ServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-mvc.xml"
})
@WebAppConfiguration
public class TestSpringMvc {

    @Autowired
    ServiceImpl service;

    @Test
    public void test() {
        service.testException();
    }
}
