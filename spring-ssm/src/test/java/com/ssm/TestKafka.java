package com.ssm;

import com.ssm.config.ServiceConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/12/11
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceConfig.class)
@WebAppConfiguration
public class TestKafka {

    @Autowired
    public KafkaTemplate<Integer, String> kafkaTemplate;

    @Test
    public void testKafkaProduct() {
        kafkaTemplate.send("springKafka","SpringKafka11");
    }

    /**
     *
     * 功能描述: 异步发送消息
     *
     * @param: []
     * @return: void
     * @author: lixiaomeng
     * @time: 2018/12/11 11:57
     */
    @Test
    public void testKafkaProductAsync() {
        ListenableFuture<SendResult<Integer, String>> listenableFuture = kafkaTemplate.send("springKafka","aaaaaa");
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Exception=="+ ex);
            }

            @Override
            public void onSuccess(SendResult<Integer, String> result) {
                System.out.println(result.getProducerRecord().toString());
                System.out.println(result.toString());
                System.out.println(result.getRecordMetadata().toString());
            }
        });
    }
}
