package com.ssm;

import com.ssm.config.WebConfig;
import com.ssm.dto.vo.UserDtoVo;
import com.ssm.service.UserServiceImpl;
import com.ssm.threadPool.AppliactionThreadPoolExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class TestUserService {

    Logger logger = LoggerFactory.getLogger(TestUserService.class);

    @Autowired
    UserServiceImpl userService;

    @Test
    public void testUser() {
        List<UserDtoVo> list = userService.userList();
        logger.info("user:[{}]",list);
        logger.info("userList:[{}]",userService.userList());
        logger.info("userList:[{}]",userService.userList());
        logger.info("userList:[{}]",userService.userList());
        Future future = AppliactionThreadPoolExecutor.getInstanceThreadPoolExecutor().submitCall(() ->{
            return userService.userList();
        });
        try {
            System.out.println(future.get());
            System.out.println("线程执行结束....");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}
