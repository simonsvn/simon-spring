package com.ssm.utils;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
public enum DateFormatEnum {
    yyyy_MM_DD_HH_mm_ss(1,"yyyy-MM-dd HH:mm:ss"),
    yyyy_MM_DD(2,"yyyy-MM-DD"),
    milliseconds(3,"milliseconds");

    private int code;
    private String value;


    DateFormatEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
