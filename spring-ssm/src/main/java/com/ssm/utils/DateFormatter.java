package com.ssm.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
public class DateFormatter {

    private static final String dateFormat_yyyy_MM_DD_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    private static final String dateFormat_yyyy_MM_DD = "yyyy-MM-dd";

    public static final String format_value = "yyyy_MM_DD_HH_mm_ss";
    public static final String regExt_yyyy_MM_DD = "^\\d{4}-\\d{2}-\\d{2}$";
    public static final String regExt_yyyy_MM_DD_HH_mm_ss = "^\\d{4}-\\d{2}-\\d{2}\\s+\\d{2}:\\d{2}:\\d{2}$";

    /* *
     *
     * 功能描述: 日期转化为字符串形式，支持一下格式
     * yyyy-MM-dd HH:mm:ss
     * yyyy-MM-dd
     * 日期毫秒数
     * @param: [time, timeFromat]
     * @return: java.lang.String
     * @author: lixiaomeng
     * @time: 2018/11/27 15:57
     */
    public static String parseDateToStr(Date date, DateFormatEnum dateFromat){
        if (DateFormatEnum.milliseconds.equals(dateFromat)) {
            return String.valueOf(date.getTime());
        } else {
            DateFormat dateFormat=new SimpleDateFormat(dateFromat.getValue());
            return dateFormat.format(date);
        }
    }

    /**
     *
     * 功能描述: 字符串格式化为日期类型，支持字符串类型为
     * yyyy-MM-dd HH:mm:ss
     * yyyy-MM-dd
     * 日期毫秒数
     * @param: [strDate]
     * @return: java.util.Date
     * @author: lixiaomeng
     * @time: 2018/11/27 15:54
     */
    public static Date parseStrToDate(String strDate) throws ParseException {
        try {
            if (StringUtils.isNumericSpace(strDate)) {
                return new Date(Long.valueOf(strDate));
            } else if (strDate.matches(regExt_yyyy_MM_DD_HH_mm_ss)) {
                SimpleDateFormat sdfTime = new SimpleDateFormat(dateFormat_yyyy_MM_DD_HH_mm_ss);
                return sdfTime.parse(strDate);
            } else if (strDate.matches(regExt_yyyy_MM_DD)) {
                SimpleDateFormat sdfTime = new SimpleDateFormat(dateFormat_yyyy_MM_DD);
                return sdfTime.parse(strDate);
            }
        } catch (ParseException e) {
            throw new ParseException("日期格式化异常:[" +strDate +"]", e.getErrorOffset());
        }
        return new Date();
    }


}
