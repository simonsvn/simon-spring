package com.ssm.controller;

import com.ssm.dto.vo.UserDtoVo;
import com.ssm.service.UserServiceImpl;
import com.ssm.utils.DateFormatEnum;
import com.ssm.utils.DateFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
@Controller
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    UserServiceImpl userService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    @ResponseBody
    public String Index() {

        return "index";
    }

    @RequestMapping(value = "dateParams",method = RequestMethod.POST)
    @ResponseBody
    public String dateParams(Date date) {
        System.out.println("日期为："+ date);
        System.out.println(DateFormatter.parseDateToStr(date, DateFormatEnum.yyyy_MM_DD_HH_mm_ss));
        return DateFormatter.parseDateToStr(date,DateFormatEnum.milliseconds);
    }

    @RequestMapping(value = "instParams", method = RequestMethod.POST)
    @ResponseBody
    public String instParams(Instant instant) {
        System.out.println("instant为：" + instant);
        System.out.println(instant.toEpochMilli());
        return String.valueOf(instant.toEpochMilli());
    }

    @RequestMapping(value = "userInfo", method = RequestMethod.GET)
    @ResponseBody
    public UserDtoVo userInfo() {
        UserDtoVo userDtoVo = new UserDtoVo();
        userDtoVo.setUsername("李李");
        userDtoVo.setDate(new Date());
        userDtoVo.setInstant(Instant.now());
        userDtoVo.setHomeUrl("http://www.baidu.com");
        return userDtoVo;
    }

    @RequestMapping(value = "userRequestBody", method = RequestMethod.POST)
    @ResponseBody
    public UserDtoVo userRequestBody( @RequestBody UserDtoVo userDtoVo) {
        System.out.println(DateFormatter.parseDateToStr(userDtoVo.getDate(),DateFormatEnum.yyyy_MM_DD_HH_mm_ss));
        System.out.println(userDtoVo);
        userDtoVo.setUsername("李李");
        return userDtoVo;
    }

    @RequestMapping(value = "userForm", method = RequestMethod.POST)
    public UserDtoVo userForm( @RequestBody String date) {
        System.out.println("date==" + date);
        UserDtoVo userDtoVo = new UserDtoVo();
        try {
            userDtoVo.setDate(DateFormatter.parseStrToDate(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userDtoVo;
    }

    @RequestMapping(value = "userList", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDtoVo> userList(@RequestBody  UserDtoVo userDtoVo) {
        List list = userService.userList();
        logger.info("userDtoVO[{}]", userDtoVo);
        logger.info("list[{}]", list);
        return list;
    }
}
