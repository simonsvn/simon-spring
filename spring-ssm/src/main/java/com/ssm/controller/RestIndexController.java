package com.ssm.controller;

import com.ssm.dto.UserDto;
import com.ssm.dto.vo.UserDtoVo;
import com.ssm.service.UserServiceImpl;
import com.ssm.threadPool.AppliactionThreadPoolExecutor;
import com.ssm.utils.DateFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.Future;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/28
 */
@RestController
public class RestIndexController {

    private static final Logger logger = LoggerFactory.getLogger(RestController.class);

    @Autowired
    UserServiceImpl userService;

    @RequestMapping(value = "restuserForm", method = RequestMethod.POST)
    public UserDtoVo userForm(String date, String username) {
        System.out.println("date==" + date);
        UserDtoVo userDtoVo = new UserDtoVo();
        try {
            userDtoVo.setDate(DateFormatter.parseStrToDate(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userDtoVo;
    }

    @RequestMapping(value = "userDtoForm", method = RequestMethod.POST)
    public UserDtoVo userDtoForm(@RequestBody UserDtoVo dtoVo) {
        logger.info("userDtoForm:",dtoVo);
        logger.info("userDtoForm:[{}]",dtoVo);
        UserDto userDto = new UserDto();
        String code = "aaa";
        String code1 = "aaa";
        logger.error("异常信息：异常:[{}],参数:[{}]", new RuntimeException(), dtoVo);
        logger.error("异常信息：异常:[{},{}]", code, code1);
        System.out.println(dtoVo);
        return dtoVo;
    }

    @RequestMapping(value = "userDtoVoList", method = RequestMethod.GET)
    public List<UserDtoVo> userDtoVoList() throws Exception {
        System.out.println("当前线程==" + Thread.currentThread().getName());
        Future<List<UserDtoVo>> future = AppliactionThreadPoolExecutor.getInstanceThreadPoolExecutor().submitCall(() ->{
            return userService.userList();
        });
        List<UserDtoVo> list = userService.userList();
        System.out.println(list);
        list.addAll(future.get());
        return list;
    }


}
