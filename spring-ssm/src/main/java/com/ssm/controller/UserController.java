package com.ssm.controller;

import com.ssm.resp.CommonDto;
import com.ssm.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/12/5
 */
@Controller
public class UserController {

    @Autowired
    public KafkaTemplate<Integer, String> kafkaTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    UserServiceImpl userService;

    @RequestMapping(value = "userPage", method = RequestMethod.GET)
    public String userIndex() {
        // redis 设置值
        stringRedisTemplate.opsForValue().set("xingming","李小蒙");

        System.out.println(stringRedisTemplate.opsForValue().get("xingming"));

        return "userIndex";
    }

    @RequestMapping(value = "fileUpload", method = RequestMethod.POST)
    @ResponseBody
    public CommonDto fileUpload(String name, MultipartFile file) {
        System.out.println(file);
        return new CommonDto();
    }

    @RequestMapping("userListPage")
    @ResponseBody
    public CommonDto userListPage() {
        CommonDto commonDto = new CommonDto();
//        commonDto.setData(userService.userPage());
        commonDto.setData(userService.userpageInfo());
        return commonDto;
    }

    @RequestMapping("kafkaMessage")
    @ResponseBody
    public String productMessage(int size, String topics) {
        for (int i = 0; i<= size; i ++) {
            kafkaTemplate.send(topics, "message: send " + i);
        }

        return "sucess";
    }

}
