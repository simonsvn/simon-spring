package com.ssm.controller.exception;

/**
 * 功能描述: Controller级别的自定义异常
 *
 * @author lixiaomeng
 * @date 2018/12/5
 */
public class ControllerException extends Exception {

    private String message; // 异常信息

    public ControllerException(String message) {
        super(message);
        this.message = message;
    }
}
