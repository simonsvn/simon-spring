package com.ssm.controller.advice;

import com.ssm.controller.exception.ControllerException;
import com.ssm.resp.CommonDto;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 功能描述: controller的异常捕获类
 *
 * @author lixiaomeng
 * @date 2018/12/5
 */
@ControllerAdvice
public class ControllerExceptionAdvice {

    @ExceptionHandler(ControllerException.class)
    public CommonDto controllerExceptionAdvice() {
        CommonDto commonDto = new CommonDto();

        return commonDto;
    }

    @ExceptionHandler(Exception.class)
    public CommonDto exceptionAdvice() {
        CommonDto commonDto = new CommonDto();

        return commonDto;
    }

}
