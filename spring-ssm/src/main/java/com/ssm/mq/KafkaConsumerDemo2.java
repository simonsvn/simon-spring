package com.ssm.mq;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerDemo2 {
    public static void main(String[] args){
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "140.143.203.127:9092");
        properties.put("group.id", "group-22");
        properties.put("enable.auto.commit", "true");
        properties.put("auto.commit.interval.ms", "1000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("session.timeout.ms", "30000");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");


        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList("partition3"));
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMinutes(1));
            System.out.println("group-6 == " + records.count());
            try {
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("group-6 = %s", record.toString());
                    Thread.sleep(500);
                    System.out.println();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
