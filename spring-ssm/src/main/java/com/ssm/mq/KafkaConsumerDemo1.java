package com.ssm.mq;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerDemo1 {
    public static void main(String[] args){
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "140.143.203.127:9092");
        properties.put("group.id", "group-21");
        properties.put("enable.auto.commit", "true");
        properties.put("auto.commit.interval.ms", "1000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("session.timeout.ms", "30000");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");


        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList("partition11")); // 客户端订阅的主题
        while (true) {

            // 客户端获取数据，这里默认使用的是批量获取，默认每次最多获取500条数据
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMinutes(1));
            System.out.println("group-5 == " + records.count());
            try {
                for (ConsumerRecord<String, String> record : records) {
                    Thread.sleep(500);
                    System.out.printf("group-5 == offset = %s, partition=%s, value = %s", record.offset(), record.partition(),record.value());
                    System.out.println();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
