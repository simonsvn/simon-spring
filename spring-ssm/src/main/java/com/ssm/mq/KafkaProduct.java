package com.ssm.mq;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class KafkaProduct {

    public static void main(String[] args){
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "140.143.203.127:9092");
        properties.put("acks", "all");
        properties.put("retries", 0);
        properties.put("batch.size", 16384);
        properties.put("linger.ms", 1);
        properties.put("buffer.memory", 33554432);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = null;
        try {
            producer = new KafkaProducer<String, String>(properties);
            for (int i = 0; i < 700; i++) {
                String msg = "partition7 Message = " + i;
                // 这里的partition是指，消息存储到那个分区上面，不是指定要创建的分区，所以partition和key是互斥的，如果设置了partition，
                // 那么设置的key值是无效的状态
//                producer.send(new ProducerRecord<String, String>("partition9", 1, i+"", msg));

//                producer.send(new ProducerRecord<String, String>("partition9", 1, null, msg));// 指定存储的分区片
//                producer.send(new ProducerRecord<String, String>("partition10", null, i+"", msg)); // 根据key的值，求hash取模
                producer.send(new ProducerRecord<String, String>("springKafka", msg)); // 分区片数，进行轮训
                System.out.println("Sent:" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            producer.close();
        }

    }

}
