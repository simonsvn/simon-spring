package com.ssm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/29
 */
@Configuration
@ComponentScan(basePackages = "com.ssm", excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class),
        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = RestController.class)
})
@Import({
        DataBaseConfig.class,
        KafkaConfig.class
})
public class ServiceConfig {
}
