package com.ssm.config;

import com.github.pagehelper.PageInterceptor;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import redis.clients.jedis.JedisPoolConfig;

import javax.sql.DataSource;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/28
 */
@Configuration
@MapperScan(basePackages = "com.ssm.mapper")
@EnableTransactionManagement
@PropertySource(value = "classpath:jdbc-properties.properties")
public class DataBaseConfig {

    @Value("${spring.datasource.url}")
    String url;

    @Value("${spring.datasource.username}")
    String username;

    @Value("${spring.datasource.password}")
    String password;

    @Value("${spring.datasource.driver-class-name}")
    String driverClassName;

    @Value("${spring.datasource.maxPoolSize}")
    int maxPoolSize;

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private int redisPort;

    @Value("${spring.redis.passwd}")
    private String redisPasswd;

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setMaximumPoolSize(maxPoolSize);
        ds.setDriverClassName(driverClassName);
        return ds;
    }


    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    /**
     *
     * 功能描述: mybatis的Configuration类，作用和mybatis-config.xml一样，控制mybatis的全局设置
     *
     * @param: []
     * @return: org.apache.ibatis.session.Configuration
     * @author: lixiaomeng
     * @time: 2018/12/12 14:44
     */
    @Bean
    public org.apache.ibatis.session.Configuration configuration() {
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();

        // 设置类属性字段和数据库字段的映射规则使用驼峰法进行映射
        configuration.setMapUnderscoreToCamelCase(true);

        // 添加mybatis的插件，这里使用分页插件
        configuration.addInterceptor(new PageInterceptor());


        configuration.setLogImpl(StdOutImpl.class);
        return configuration;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
        factory.setDataSource(dataSource());
        factory.setTypeAliasesPackage("com.ssm.dto");
        ResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
        Resource[] mapperLocations=resolver.getResources("classpath:com/ssm/mapper/*.xml");
        factory.setMapperLocations(mapperLocations);
        factory.setConfiguration(configuration());
        return factory.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
        return sqlSessionTemplate;
    }


    // redsi集成

    public JedisPoolConfig poolConfig() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(100);
        poolConfig.setMaxIdle(100);
        poolConfig.setMaxWaitMillis(2000);
        return poolConfig;
    }


    @Bean(value = "jedisConnectionFactory")
    public JedisConnectionFactory connectionFactory() {
        RedisStandaloneConfiguration standaloneConfiguration = new RedisStandaloneConfiguration(redisHost,redisPort);
        standaloneConfiguration.setPassword(redisPasswd);
        JedisClientConfiguration.JedisPoolingClientConfigurationBuilder jpcf =
                (JedisClientConfiguration.JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
        jpcf.poolConfig(poolConfig());
        JedisClientConfiguration jedisClientConfiguration = jpcf.build();
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory(standaloneConfiguration,jedisClientConfiguration);
        return connectionFactory;
    }

    @Bean
    public StringRedisTemplate stringRedisTemplate() {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(connectionFactory());

        return stringRedisTemplate;

    }




}
