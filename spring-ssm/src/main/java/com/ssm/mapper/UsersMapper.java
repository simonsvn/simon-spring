package com.ssm.mapper;

import com.ssm.dto.vo.UserDtoVo;

import java.util.List;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/28
 */
public interface UsersMapper {

    public List<UserDtoVo> selectUser();
}
