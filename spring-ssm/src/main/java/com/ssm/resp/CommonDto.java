package com.ssm.resp;

import java.io.Serializable;

/**
 * 功能描述: 返回数据的包装类
 *
 * @author lixiaomeng
 * @date 2018/12/5
 */
public class CommonDto implements Serializable {

    private int code;

    private String message;

    private Object data;

    private String exceptionMsg;

    public CommonDto() {
        this.code = 1; // 默认CODE=1，成功状态
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getExceptionMsg() {
        return exceptionMsg;
    }

    public void setExceptionMsg(String exceptionMsg) {
        this.exceptionMsg = exceptionMsg;
    }

    @Override
    public String toString() {
        return "CommonDto{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", exceptionMsg='" + exceptionMsg + '\'' +
                '}';
    }
}
