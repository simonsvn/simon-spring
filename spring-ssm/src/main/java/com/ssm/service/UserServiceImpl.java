package com.ssm.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssm.dto.vo.UserDtoVo;
import com.ssm.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/28
 */
@Service
public class UserServiceImpl {

    @Autowired
    UsersMapper usersMapper;

    public List<UserDtoVo> userList() {
        System.out.println("userList==="+ Thread.currentThread().getName());
        return usersMapper.selectUser();
    }

    /**
     *
     * 功能描述:  分页查询设置，使用分页插件提供的doSelect方法查询，该方法是安全的，不存在分页查询，线程参数
     * 乱掉的问题
     *
     * @param: []
     * @return: com.github.pagehelper.Page<com.ssm.dto.vo.UserDtoVo>
     * @author: lixiaomeng
     * @time: 2018/12/12 14:25
     */
    public Page<UserDtoVo> userPage() {
        Page<UserDtoVo> page =  PageHelper.startPage(1, 10).doSelectPage(() ->{
           usersMapper.selectUser();
        });
        return page;
    }

    /**
     *
     * 功能描述: 返回查询结果集的PageInfo包装类，查询结果在 PageInfo类中的list中，
     * PageInfo中，包含了查询页码，查询条数，总条数，当前是第几页，总共多少页，是否是最后一页，是否是第一页等信息
     *
     * @param: []
     * @return: com.github.pagehelper.PageInfo<com.ssm.dto.vo.UserDtoVo>
     * @author: lixiaomeng
     * @time: 2018/12/12 15:09
     */
    public PageInfo<UserDtoVo> userpageInfo() {
        PageInfo pageInfo =  PageHelper.startPage(1,10).doSelectPageInfo(() ->{
            usersMapper.selectUser();
        });
        return pageInfo;
    }


    /**
     *
     * 功能描述: 直接使用分页插件，返回查询结果的list集合
     *
     * @param: []
     * @return: java.util.List<com.ssm.dto.vo.UserDtoVo>
     * @author: lixiaomeng
     * @time: 2018/12/12 14:50
     */
    public List<UserDtoVo> listUser() {
        // 直接设置分页查询的页码和查询条数，mapper的查询语句，必须紧跟在该语句后面
        PageHelper.startPage(1,10);
        List<UserDtoVo> list = usersMapper.selectUser();
        return list;
    }
}
