package com.ssm.dto.vo;

import com.ssm.dto.UserDto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
public class UserDtoVo extends UserDto implements Serializable {

    private Date date;

    private Instant instant;

    private String homeUrl;

    private String screenName;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Instant getInstant() {
        return instant;
    }

    public void setInstant(Instant instant) {
        this.instant = instant;
    }


    @Override
    public String getHomeUrl() {
        return homeUrl;
    }

    @Override
    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
}
