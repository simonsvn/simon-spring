package com.ssm.dto;

import java.io.Serializable;

/**
 * 功能描述:
 *
 * @author lixiaomeng
 * @date 2018/11/27
 */
public class UserDto implements Serializable {

    private Long uid;

    private String username;
    private String email;
    private String homeUrl;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeUrl() {
        return homeUrl;
    }

    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", homeUrl='" + homeUrl + '\'' +
                '}';
    }
}
